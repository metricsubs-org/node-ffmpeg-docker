FROM node:latest

RUN set -eux; \
    apt-get update; \
    apt-get -y install ffmpeg; \
    apt-get -y install wget;
